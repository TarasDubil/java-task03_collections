package com.dubilok.model.deque;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyDeque<E> implements Deque<E> {

    private static Logger logger = LogManager.getLogger(MyDeque.class);
    private List<E> list = new ArrayList<>();
    private int head = 0;

    public MyDeque() {
        logger.info("MyDeque was created successful");
    }

    @Override
    public void addFirst(E e) {
        list.add(head, e);
        logger.info("successful added element: " + e + " in head");
    }

    @Override
    public void addLast(E e) {
        logger.info("successful added element: " + e + " in tail");
        list.add(e);
    }

    @Override
    public boolean offerFirst(E e) {
        if (e != null) {
            addFirst(e);
            logger.info("successful added element: " + e);
            return true;
        }
        return false;
    }

    @Override
    public boolean offerLast(E e) {
        if (e != null) {
            addLast(e);
            logger.info("successful added element: " + e);
            return true;
        }
        logger.info("Not successful added element: " + e);
        return false;
    }

    @Override
    public E removeFirst() {
        logger.info("successful deleted element: " + list.get(head));
        return removeElement(head);
    }

    @Override
    public E removeLast() {
        logger.info("successful deleted element: " + list.get(list.size()-1));
        return removeElement(list.size()-1);
    }

    private E removeElement(int position) {
        if (list.isEmpty()) {
            logger.info("MyDeque is empty");
            throw new NoSuchElementException();
        }
        E element = list.get(position);
        list.remove(position);
        logger.info("successful removed element: " + element + " from position: " + position);
        return element;
    }

    @Override
    public E pollFirst() {
        return removeElementWithOutNull(head);
    }

    @Override
    public E pollLast() {
        return removeElementWithOutNull(list.size()-1);
    }

    private E removeElementWithOutNull(int position) {
        if (list.isEmpty()) {
            logger.info("MyDeque is empty");
            return null;
        }
        E element = list.get(position);
        logger.info("successful removed element: " + element + " from position: " + position);
        remove(position);
        return element;
    }

    @Override
    public E getFirst() {
        return getElementFromList(head);
    }

    @Override
    public E getLast() {
        return getElementFromList(list.size()-1);
    }

    private E getElementFromList(int position) {
        if (list.isEmpty()) {
            logger.info("MyDeque is empty");
            throw new NoSuchElementException();
        }
        logger.info("successful get element: " + list.get(position) + " from position: " + position);
        return list.get(position);
    }

    @Override
    public E peekFirst() {
        return getElementFromListWithOutNull(head);
    }

    @Override
    public E peekLast() {
        return getElementFromListWithOutNull(list.size()-1);
    }

    private E getElementFromListWithOutNull(int position) {
        if (list.isEmpty()) {
            logger.info("MyDeque is empty");
            return null;
        }
        logger.info("successful get element: " + list.get(position) + " from position: " + position);
        return list.get(position);
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        int size = list.size();
        logger.info("The first occurrence element was deleted");
        list.remove(o);
        if (list.size() < size) {
            return true;
        }
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        int size = list.size();
        for (int i = size-1; i >= 0; i--) {
            if (list.get(i).equals(o)) {
                logger.info("The las occurrence element was deleted");
                list.remove(i);
                break;
            }
        }
        if (list.size() < size) {
            return true;
        }
        return false;
    }

    @Override
    public boolean add(E e) {
        addLast(e);
        return true;
    }

    @Override
    public boolean offer(E e) {
        return offerLast(e);
    }

    @Override
    public E remove() {
        return removeFirst();
    }

    @Override
    public E poll() {
        return pollFirst();
    }

    @Override
    public E element() {
        return getFirst();
    }

    @Override
    public E peek() {
        return peekFirst();
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return list.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return list.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public void clear() {
        logger.info("MyDeque was cleaned");
        list.clear();
    }

    @Override
    public void push(E e) {
        addFirst(e);
    }

    @Override
    public E pop() {
        return getFirst();
    }

    @Override
    public boolean remove(Object o) {
        return removeFirstOccurrence(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public int size() {
        logger.info("size of MyDeque: " + list.size());
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        logger.info("MyDeque is empty -> " + (list.isEmpty() ? " Yes" : " No"));
        return list.isEmpty();
    }

    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    @Override
    public Iterator<E> descendingIterator() {
        LinkedList<E> linkedList = (LinkedList<E>) list;
        return linkedList.descendingIterator();
    }
}
