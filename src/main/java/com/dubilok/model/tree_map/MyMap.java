package com.dubilok.model.tree_map;

public interface MyMap<K, V> {
    int size();

    boolean isEmpty();

    String toString();

    void show();

    V get(Object key);

    V put(K key, V value);

    V remove(Object key);

    void clear();

    boolean containsKey(Object key);
}
