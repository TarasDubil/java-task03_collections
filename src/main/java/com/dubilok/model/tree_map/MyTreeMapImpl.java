package com.dubilok.model.tree_map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

public class MyTreeMapImpl<K, V> implements MyMap<K, V> {

    private static Logger logger = LogManager.getLogger(MyTreeMapImpl.class);
    private Entry<K, V> root;
    private int size = 0;

    public MyTreeMapImpl() {
        logger.info("MyTreeMap was created successful.");
        root = new Entry<>(null, null);
    }

    static final class Entry<K, V> implements Comparable<K> {
        K key;
        V value;
        Entry<K, V> left;
        Entry<K, V> right;
        Entry<K, V> parent;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            logger.info("successful get key from MyTreeMap: " + key);
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            logger.info("successful get value from MyTreeMap: " + value);
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) return true;
            if (object == null || getClass() != object.getClass()) return false;
            Entry<?, ?> entry = (Entry<?, ?>) object;
            return Objects.equals(key, entry.key) &&
                    Objects.equals(value, entry.value);
        }

        @Override
        public int hashCode() {
            return (key == null ? 0 : key.hashCode());
        }

        public String toString() {
            return key + "=" + value;
        }

        @Override
        public int compareTo(K o) {
            Entry<K, V> node = new Entry<>(o, null);
            return this.hashCode() - node.hashCode();
        }
    }

    @Override
    public int size() {
        logger.info("size of MyTreeMap = " + size);
        return size;
    }

    @Override
    public boolean isEmpty() {
        logger.info("MyTreeMap is empty = " + (size == 0));
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        Entry<K, V> eEntry = new Entry<>((K) key, null);
        Entry<K, V> search = search(root, eEntry);
        if (search.key == eEntry.key) {
            logger.info("MyTreeMap contains Key '" + key + "'");
            return true;
        }
        logger.info("MyTreeMap not contains Key '" + key + "'");
        return false;
    }

    @Override
    public String toString() {
        return inOrder(root);
    }

    @Override
    public void show() {
        logger.info(inOrder(root) + "Your Map");
    }

    private String inOrder(Entry<K, V> localRoot) {
        StringBuilder string = new StringBuilder();
        if (size == 0) {
            return "[]";
        }
        if (localRoot != null) {
            inOrder(localRoot.left);
            System.out.println("[Key: " + localRoot.key + "; Value: " + localRoot.value + "]");
            inOrder(localRoot.right);
        }
        return string.toString();
    }

    @Override
    public V get(Object key) {
        if (isEmpty()) {
            return null;
        }
        Entry<K, V> eEntry = new Entry<>((K) key, null);
        V value = search(root, eEntry).value;
        if (value == null) {
            logger.info("you cannot get value!");
        } else {
            logger.info("you successful get value: " + value);
        }
        return value;
    }

    private Entry<K, V> search(Entry<K, V> entry, Entry<K, V> eEntry) {
        int compare = eEntry.compareTo((K) eEntry);
        if (compare < 0 && entry.right != null) {
            return search(entry.right, eEntry);
        }
        if (compare > 0 && entry.left != null) {
            return search(entry.left, eEntry);
        }
        if (compare == 0) {
            return entry;
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        logger.info("You have successful added " + key + "=" + value);
        if (size == 0) {
            return initRootEntry(key, value);
        }
        Entry<K, V> newEntry = new Entry<>(key, value);
        Entry<K, V> lastEntry = findLastEntry(root, newEntry);
        if (lastEntry == null) {
            return null;
        }
        if (lastEntry.compareTo((K) newEntry) == 0) {
            lastEntry.value = value;
            return null;
        } else {
            size++;
            newEntry.parent = lastEntry;
            if (lastEntry.compareTo((K) newEntry) < 0) {
                lastEntry.right = newEntry;
                return null;
            } else {
                lastEntry.left = newEntry;
                return null;
            }
        }
    }

    private V initRootEntry(final K key, final V value) {
        root.setKey(key);
        root.setValue(value);
        size++;
        return value;
    }

    private Entry<K, V> findLastEntry(final Entry<K, V> oldEntry,
                                      final Entry<K, V> newEntry) {
        Entry<K, V> lastEntry = oldEntry;
        int compare = oldEntry.compareTo((K) newEntry);
        if (compare < 0 && oldEntry.right != null) {
            lastEntry = findLastEntry(oldEntry.right, newEntry);
            return lastEntry;
        }
        if (compare > 0 && oldEntry.left != null) {
            lastEntry = findLastEntry(oldEntry.left, newEntry);
            return lastEntry;
        }
        if (compare == 0) {
            return lastEntry;
        }
        return lastEntry;
    }

    @Override
    public V remove(Object key) {
        Entry<K, V> forComparing = new Entry<>((K) key, null);
        Entry<K, V> focusNode = root;
        Entry<K, V> parent = root;
        boolean isItALeftChild = true;
        size--;
        while (focusNode.key != key) {
            parent = focusNode;
            if (forComparing.compareTo((K) focusNode) < 0) {
                isItALeftChild = true;
                focusNode = focusNode.left;
            } else {
                isItALeftChild = false;
                focusNode = focusNode.right;
            }
            if (focusNode == null) {
                logger.info("you have successful removed " + key);
                return null;
            }
        }
        if (focusNode.left == null && focusNode.right == null) {
            if (focusNode == root) {
                root = null;
            } else if (isItALeftChild) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } else if (focusNode.right == null) {
            if (focusNode == root) {
                root = focusNode.left;
            } else if (isItALeftChild) {
                parent.left = focusNode.left;
            } else {
                parent.right = focusNode.left;
            }
        } else if (focusNode.left == null) {
            if (focusNode == root) {
                root = focusNode.right;
            } else if (isItALeftChild) {
                parent.left = focusNode.right;
            } else {
                parent.right = focusNode.right;
            }
        } else {
            Entry<K, V> replacement = getReplacementNode(focusNode);
            if (focusNode == root) {
                root = replacement;
            } else if (isItALeftChild) {
                parent.left = replacement;
            } else {
                parent.right = replacement;
            }
            replacement.left = focusNode.left;
        }
        logger.info("you have not successful removed " + key);
        return null;
    }

    private Entry<K, V> getReplacementNode(Entry<K, V> replacedNode) {
        Entry<K, V> replacementParent = replacedNode;
        Entry<K, V> replacement = replacedNode;
        Entry<K, V> focusNode = replacedNode.right;
        while (focusNode != null) {
            replacementParent = replacement;
            replacement = focusNode;
            focusNode = focusNode.left;
        }
        if (replacement != replacedNode.right) {
            replacementParent.left = replacement.right;
            replacement.right = replacedNode.right;

        }
        return replacement;
    }

    @Override
    public void clear() {
        logger.info("You have successful cleared MyTreeMap");
        size = 0;
        root = new Entry<>(null, null);
    }

}
