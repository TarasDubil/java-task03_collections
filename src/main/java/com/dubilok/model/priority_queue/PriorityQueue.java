package com.dubilok.model.priority_queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PriorityQueue<E> extends AbstractQueue<E> {

    private final int INITIAL_CAPACITY = 16;
    private static Logger logger = LogManager.getLogger(PriorityQueue.class);
    private int initialCapacity;
    private List<E> list = new ArrayList<>(initialCapacity);

    public PriorityQueue() {
        logger.info("created PriorityQueue with size: " + INITIAL_CAPACITY);
        this.initialCapacity = INITIAL_CAPACITY;
    }

    public PriorityQueue(int initialCapacity) {
        logger.info("created PriorityQueue with size: " + initialCapacity);
        this.initialCapacity = initialCapacity;
    }

    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

    @Override
    public int size() {
        logger.info("PriorityQueue size is: " + list.size());
        return list.size();
    }

    @Override
    public boolean offer(E e) {
        logger.info("was added element : " + e);
        return list.add(e);
    }

    @Override
    public E poll() {
        E element = null;
        if (!list.isEmpty()) {
            element = list.get(0);
        }
        if (element == null) {
            logger.info("element was not deleted");
            return null;
        }
        logger.info("element was deleted: " + list.get(0));
        list.remove(0);
        return element;
    }

    @Override
    public E peek() {
        if (list.isEmpty()) {
            logger.info("PriorityQueue is empty!");
            return null;
        }
        logger.info("Get element " + list.get(0));
        return list.get(0);
    }
}
