package com.dubilok.model.tree_set;

import java.util.List;

public interface Tree<E> extends Iterable<E> {

    boolean add(E e);

    int size();

    List<E> get();

    SimpleTree.Leaf find(E e);
}
