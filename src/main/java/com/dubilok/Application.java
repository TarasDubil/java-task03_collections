package com.dubilok;

import com.dubilok.view.ViewImpl;

public class Application {

    public static void main(String[] args) {
        new ViewImpl().show();
    }

}

