package com.dubilok.service;

import com.dubilok.model.deque.MyDeque;
import com.dubilok.model.priority_queue.PriorityQueue;
import com.dubilok.util.UtilMenu;
import com.dubilok.view.Printable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ManagerMyDeque implements Manager {

    private MyDeque<Integer> myDeque;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ManagerMyDeque() {
        myDeque = new MyDeque<>();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - add element");
        menu.put("2", "  2 - get first element");
        menu.put("3", "  3 - get last element");
        menu.put("4", "  4 - remove first occurrence element");
        menu.put("5", "  5 - remove last occurrence element");
        menu.put("6", "  6 - remove element");
        menu.put("7", "  7 - clear");
        menu.put("8", "  8 - show size");
        menu.put("9", "  9 - show all elements");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
        methodsMenu.put("9", this::pressButton9);
    }

    private void pressButton1() {
        System.out.println("Enter your Integer element:");
        try {
            myDeque.add(Integer.valueOf(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() {
        myDeque.getFirst();
    }

    private void pressButton3() {
        myDeque.getLast();
    }

    private void pressButton4() {
        System.out.println("Enter your Integer element:");
        try {
            myDeque.removeFirstOccurrence(Integer.valueOf(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton5() {
        System.out.println("Enter your Integer element:");
        try {
            myDeque.removeLastOccurrence(Integer.valueOf(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton6() {
        System.out.println("Enter your Integer element:");
        try {
            myDeque.remove(Integer.valueOf(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton7() {
        myDeque.clear();
    }

    private void pressButton8() {
        myDeque.size();
    }

    private void pressButton9() {
        myDeque.forEach(System.out::println);
    }

    @Override
    public void showMethods() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
