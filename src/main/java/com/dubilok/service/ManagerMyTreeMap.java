package com.dubilok.service;

import com.dubilok.model.tree_map.MyTreeMapImpl;
import com.dubilok.util.UtilMenu;
import com.dubilok.view.Printable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ManagerMyTreeMap implements Manager {

    private MyTreeMapImpl<Integer, String> map;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ManagerMyTreeMap() {
        map = new MyTreeMapImpl<>();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - add element");
        menu.put("2", "  2 - show size");
        menu.put("3", "  3 - isEmpty");
        menu.put("4", "  4 - contains Key");
        menu.put("5", "  5 - get value by Key");
        menu.put("6", "  6 - show all elements");
        menu.put("7", "  7 - clear");
        menu.put("8", "  8 - remove element");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
    }

    private void pressButton1() {
        try {
            System.out.println("Enter your Integer key:");
            Integer key = Integer.valueOf(bufferedReader.readLine());
            System.out.println("Enter your String value:");
            String value = bufferedReader.readLine();
            map.put(key, value);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() {
        map.size();
    }

    private void pressButton3() {
        map.isEmpty();
    }

    private void pressButton4() {
        System.out.println("Enter your key:");
        try {
            map.containsKey(Integer.parseInt(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton5() {
        System.out.println("Enter your key:");
        try {
            map.get(Integer.parseInt(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton6() {
        map.show();
    }

    private void pressButton7() {
        map.clear();
    }

    private void pressButton8() {
        System.out.println("Enter your key:");
        try {
            map.remove(Integer.parseInt(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showMethods() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
