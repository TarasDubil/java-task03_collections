package com.dubilok.service;

import com.dubilok.model.deque.MyDeque;
import com.dubilok.model.priority_queue.PriorityQueue;
import com.dubilok.model.tree_map.MyTreeMapImpl;
import com.dubilok.util.UtilMenu;
import com.dubilok.view.Printable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ManagerPriorityQueue implements Manager {

    private PriorityQueue<Integer> priorityQueue;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ManagerPriorityQueue() {
        priorityQueue = new PriorityQueue<>();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - add elements to Priority Queue");
        menu.put("2", "  2 - show size");
        menu.put("3", "  3 - get first element and delete");
        menu.put("4", "  4 - get first element");
        menu.put("5", "  5 - show all elements");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        System.out.println("Enter your Integer element:");
        try {
            priorityQueue.offer(Integer.valueOf(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() {
        priorityQueue.size();
    }

    private void pressButton3() {
        priorityQueue.poll();
    }

    private void pressButton4() {
        priorityQueue.peek();
    }

    private void pressButton5() {
        priorityQueue.forEach(System.out::println);
    }

    @Override
    public void showMethods() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
