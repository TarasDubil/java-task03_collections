package com.dubilok.controller;

import com.dubilok.service.Manager;
import com.dubilok.service.ManagerMyDeque;
import com.dubilok.service.ManagerMyTreeMap;
import com.dubilok.service.ManagerPriorityQueue;

public class ControllerImpl implements Controller {

    private Manager managerDeque;
    private Manager managerPriorityQueue;
    private Manager managerTreeMap;

    public ControllerImpl() {
        managerDeque = new ManagerMyDeque();
        managerPriorityQueue = new ManagerPriorityQueue();
        managerTreeMap = new ManagerMyTreeMap();
    }

    @Override
    public void showMyDequeMethods() {
        managerDeque.showMethods();
    }

    @Override
    public void showPriorityQueueMethods() {
        managerPriorityQueue.showMethods();
    }

    @Override
    public void showMyTreeMapMethods() {
        managerTreeMap.showMethods();
    }
}
