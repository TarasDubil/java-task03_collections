package com.dubilok.controller;

public interface Controller {
    void showMyDequeMethods();

    void showPriorityQueueMethods();

    void showMyTreeMapMethods();
}
