package com.dubilok.view;

public interface View {
    void show();
}
